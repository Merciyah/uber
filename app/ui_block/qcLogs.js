import React from 'react';
import { StyleSheet, TouchableOpacity, UIManager, Platform, LayoutAnimation, Dimensions, Text, ListView, View } from 'react-native';
import projects from '../data_exchange/lib/qcprojects'
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from "react-native-modal";
import ProjectLogs from "./widgets/projectLogs";
import Navbar from './widgets/nav'
import colors from '../data_exchange/lib/color'

var {height, width} = Dimensions.get('window');
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
var priy = [{color:'#36aa7b', name:'Low'}, {color:'#ebd045', name:'Med.'}, {color:'#b04148', name:'High'}]
var progress = [{color:'#000', queue:'low'},{color:'#000', queue:'low'}, {color:'#000', queue:'low'},{color:'#000', queue:'low'}]
var colorScheme = colors
    var color1 = colors[0]
    var color2 = colors[3]

export default class QCprojects extends React.Component {
constructor(props){
	super(props)

   if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental &&   UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  this.state = {
    dataSource : ds.cloneWithRows(projects),
    progress: progress,
    isModal1Visible: false,
    modalData: null
  };
}


  on(val){
    this.setState({
      isModal1Visible: true,
      modalData: val
    })
  }

  close(){
     this.setState({
      isModal1Visible: false
    })
  }



  progressBar(posih, name){
    return(<View style={{flex:1, flexDirection:'row', alignItems:'center', justifyContent:'flex-start'}}>
      <View style={{width:30, height:30, borderWidth:0, borderColor:color2, backgroundColor:colorScheme[posih+1], borderRadius:15, alignItems:'center', justifyContent:'center'}}>
      <Text style={{color:color2, fontSize:12}}>{name.substring(0,1)}</Text>
      </View>
      </View>)
  }

    messages(size){

      return(<View style={{flexDirection:'row', alignItems:'center', height:30}}><Icon name = "envelope-o" size={15} color="#ccc" style={{margin:5}} /><View style={{backgroundColor:color2, justifyContent:'center', alignItems:'center', width:12, height:12, marginLeft:-10, marginTop:-5, borderRadius:6}}><Text style={{color:color1, fontSize:7, alignSelf:'center', textAlign:'center', justifyContent:'center'}}>{size}</Text></View></View>)
    }

    priority(x){
      return(<View style={{flexDirection:"row", alignItems:'center'}}>
      <View style={{width:6, height:6, borderRadius:3, margin:10,  backgroundColor:priy[x].color }} />
      <Text style={{fontSize:10, color:color2}}>{priy[x].name} Priority</Text>
      </View>)
    }

	eachRow(project){

    if(project.active){
      color1 = colors[4]
      color2 = colors[0]
    }else{
      color1 = colors[0]
      color2 = colors[4]
    }
    console.log(project)
		return(<TouchableOpacity onPress ={() => this.on(project)} style={{height:55, flexDirection:'row', 
      padding:10,
      margin:2,
      backgroundColor:color1,
      borderColor:'#f4f4f4',
      borderWidth:1,
      borderRadius:10,
      shadowColor: '#f3f3f3', justifyContent:'space-between', alignItems:'center',}}>
      {this.progressBar(project.Progress, project.deligate)}
      <Text style={{fontSize:11, color:'#b7b7b7'}}># {project.id}</Text>
      {this.priority(project.Priority)}
      {this.messages(project.Notes.length)}
			</TouchableOpacity>)
	}
	

  render(){
    LayoutAnimation.easeInEaseOut();

    return (
    <View style={{paddingTop:0}}>
    <Navbar val={1} navTop ={this.props.activeNavBar} navigator={this.props.navigator} />
       <Text style={{alignSelf:"flex-start", color:colors[0], fontSize:23, fontWeight:"300",margin:20, }}>QC Logs</Text>
       <Text style={{alignSelf:"flex-start", color:colors[0], fontSize:10, fontWeight:'200', margin:20, marginTop:0}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce eget auctor neque. Ut porttitor ornare risus, id rhoncus augue gravida at.</Text>
      <ListView 
      showsVerticalScrollIndicator={false}
      dataSource = {this.state.dataSource}
      renderRow = {(data) => this.eachRow(data)}
      />
<Modal
          isVisible={this.state.isModal1Visible}
          backdropColor='rgba(0,0,0,0.4)'
          backdropOpacity={1}
          animationIn="slideInDown"
          animationOut="slideOutUp"

        >
          <ProjectLogs uiData = {this.state.modalData} close = {() => this.close()} />
        </Modal>
        <TouchableOpacity onPress={this.props.queueNav} style={{ position:'absolute', right:0, top:80, borderRadius:15, margin:10, marginTop:this.props.activeNavBar, backgroundColor:colorScheme[4], width:40, height:40, justifyContent:'center', alignItems:'center'}}>
      <Icon name = {this.props.menu}  size = {27} style={{}} color ={colorScheme[0]} />
       </TouchableOpacity>
      </View>);
  }
} 