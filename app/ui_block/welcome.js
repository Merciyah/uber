/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  UIManager,
  Platform,
  ImageBackground,
  LayoutAnimation,
  TextInput,
  Dimensions,
  Image,
  Text,
  View
} from 'react-native';
import Video from 'react-native-video';
var {height, width} = Dimensions.get('window');

import logo1 from '../imgs/logo.png'
import logo2 from '../imgs/logo.png'

import back1 from '../imgs/stock1.jpg'

import back4 from '../imgs/back9.jpg'
import vid from '../vids/background.mp4'
//CSS for login Actice
const regActive = {
  loginView:{flex:1, width:null, height:null, justifyContent:'center'},
  loginInfo:{height:0},
  regInfo:{height:230,},
  input: {height:0, width:0,},
  inputR: {height:40, borderWidth:1, borderColor:'#d7d7d7', width:180, fontSize:11, padding:10,margin:5, backgroundColor:'rgba(255,255,255,0.5)', borderRadius:5},
  registerView:{flex:8, width:null, height:null, alignItems:'center', justifyContent:'center'},
  logo:{width:0, height:0, margin:0},
  logoR: {width:150, height:70, margin:20},
  loginText: {transform: [{rotate:"90deg"}], width:45,height:80, marginLeft:-35, fontSize:13, color:'#fff', fontWeight:'700'},
  registerText: {transform: [{rotate:"0deg"}], width:75, fontSize:13, color:'#fff', fontWeight:'700'},

}
// Register Active
loginActive = {
loginView:{flex:8, width:null, height:null, alignItems:'center', justifyContent:'center'},
  registerView:{flex:1, width:null, height:null, alignItems:'center', justifyContent:'center'},
 loginInfo:{height:130,},
  regInfo:{height:0},
  logo:{width:150, height:70, margin:20},
  logoR: {width:0, height:0, margin:0},
  inputR: {height:0, width:0,},
  input: {height:40, width:180, fontSize:11, padding:10,margin:5, backgroundColor:'rgba(255,255,255,0.5)', borderRadius:5},
  registerText:{transform: [{rotate:"90deg"}], width:75,height:80, marginLeft:-60, fontSize:13, color:'#fff', fontWeight:'700'},
  loginText: {transform: [{rotate:"0deg"}], width:45, fontSize:13, color:'#fff', fontWeight:'700'},

}



export default class Welcome extends Component {
  constructor(props){
    super(props)
    this.state = {
      activeCSS : loginActive,
      regBack: back1,
      loginBack: back4,
      vidLeft: -200,
    }

    if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental &&   UIManager.setLayoutAnimationEnabledExperimental(true);
}
  }

  inputForm(holder, state, style){
    return(<TextInput 
      style = {style}
      placholder = {holder}

      />)
  }

  _login(){
    if(this.state.activeCSS == loginActive){
        this.props.navigator.push({
          id: 'book',
          name: 'book'
        })}else{
          this.setState({activeCSS:loginActive})
        }
  }
  
  login(){
    LayoutAnimation.easeInEaseOut();
    return(<ImageBackground source={this.state.loginBack} opacity = {0.7} resizeMode="cover" style={this.state.activeCSS.loginView}>
            <Image source = {logo1} resizeMode='contain' style ={this.state.activeCSS.logo} />

      <View style={this.state.activeCSS.loginInfo}>
            <TextInput
      underlineColorAndroid = "rgba(0,0,0,0)"
      style={this.state.activeCSS.input}
      placeholder="Username"
      placeholderTextColor = "#fff"
      value = {this.state.user}
      />
      <TextInput
      underlineColorAndroid = "rgba(0,0,0,0)"
      style={this.state.activeCSS.input}
      placeholder="Password"
      placeholderTextColor = "#fff"
      value = {this.state.user}
      />
      </View>
      <TouchableOpacity onPress={() => this._login()}><Text style={this.state.activeCSS.loginText}>LOG IN</Text></TouchableOpacity>
      </ImageBackground>)
    }
  
  register(){
    LayoutAnimation.easeInEaseOut();
    return(<ImageBackground source ={this.state.regBack} opacity = {0.8} resizeMose="cover" style={this.state.activeCSS.registerView}>
      <Image source = {logo2} resizeMode='contain' style ={this.state.activeCSS.logoR} />
      <View style={this.state.activeCSS.regInfo}>
      <TextInput
      underlineColorAndroid = "rgba(0,0,0,0)"
      style={this.state.activeCSS.inputR}
      placeholder="Username"
      placeholderTextColor = "#777"
      value = {this.state.user}
      />
      <TextInput
      underlineColorAndroid = "rgba(0,0,0,0)"
      style={this.state.activeCSS.inputR}
      placeholder="Password"
      placeholderTextColor = "#777"
      value = {this.state.user}
      />
      <TextInput
      underlineColorAndroid = "rgba(0,0,0,0)"
      style={this.state.activeCSS.inputR}
      placeholder="Confrim Password"
      placeholderTextColor = "#777"
      value = {this.state.user}
      />
      <TextInput
      underlineColorAndroid = "rgba(0,0,0,0)"
      style={this.state.activeCSS.inputR}
      placeholder="Goal"
      placeholderTextColor = "#777"
      value = {this.state.user}
      />
      </View>
      <TouchableOpacity onPress={() => this.setState({activeCSS:regActive})}><Text style={this.state.activeCSS.registerText}>REGISTER</Text></TouchableOpacity>
      </ImageBackground>)
  }

  swap(){
    this.setState = {
      activeCSS:regActive
    }
  }
  render() {
    return (
        <View style={{flex:1}}>
       <View
       style={{width:width+200, height:height, position:'absolute', left:this.state.vidLeft}} />
       <View style={{flex:1,   flexDirection:"row", backgroundColor:'rgba(0,0,0,0.7)'}}>
       {this.login()}
        {this.register()}
       </View>
       </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

