import React from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';

import Navigator from './navigator'
import colors from '../data_exchange/lib/color'



export default class Arc extends React.Component {
  render() {
    return (
      <View style={{flex:1, backgroundColor:colors[4], paddingTop:20}}>
         <StatusBar hidden ={false} barStyle={'light-content'} translucent={true} />
<Navigator />
      </View>
    );
  }
} 