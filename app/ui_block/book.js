import React from 'react';
import { StyleSheet, Dimensions, UIManager, Platform, Text, View, ScrollView, TextInput, TouchableOpacity, StatusBar, LayoutAnimation   } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import avatar from '../data_exchange/mock' 
import port from '../data_exchange/lib/ports'
import Navbar from './widgets/nav'
import Shervil from '../data_exchange/shervil'

var colorScheme = ["#fff", "#5D7A8F", "#C3936A", "#B4A59A", "#20435D"]
var {height, width} = Dimensions.get('window');

var beach = port.USOAK;

export default class Book extends React.Component {
	constructor(props){
		super(props)

		this.state = {
      setAmount: true,
      setDuration: true,
      rotate: "0deg",
      pointOfInterest: {
        val : 1287.97,
        keyboard: 'numeric',
        title: "Amount",
        subText:'Your pricing estimate is based on the blah blah blah factor. This means that for everytime you blah blah, you get a blah blah (copywritting gig).'
      },
      duration: {
        val : 100,
        title: "Duration Estaimates",
        keyboard: "numeric",
        subText:"Your timing estimate means how much time is going to go into each product inspection. The heavier the package, the more time it's gonna take to do a complete diagnostic."
      }

		}
    // This is for android Animations
    if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental &&   UIManager.setLayoutAnimationEnabledExperimental(true);
}
	}


    // Init Shervil data flow
    _getAmount(){

    }

    _getDuration(){

    }

    _getLocation(){

    }
    // Shervil dataflow ends here

  	_close(){
  		// console.log("close icon test")
  	}
  	_bookThis(){
  		// console.log("close icon test")
  	}
  	
  	_close(){
  		// console.log("close icon test")
  	}

    componentDidMount(){

      
    }

    _amount(){

    }

    _timejs(){

    }

    _changeProperties(){

    }

    _queueNav(){

    }


  	 render(){
      console.log(Shervil)
       LayoutAnimation.easeInEaseOut();
    return (
      <View style={{flex:1, backgroundColor:colorScheme[0]}}>
       <Navbar val={0} navTop ={this.props.activeNavBar} navigator={this.props.navigator} />

    	<ScrollView showsVerticalScrollIndicator={false} bounces ={false} ref ={"newScroll"} contentContainerStyle={{marginBottom:0}} style={{flex:3, margin:10, marginTop:-10}}>
		     <View style={{flex:2, justifyContent:'center',paddingTop:20}}>
		      <Text style={{fontWeight:"300", padding:10, marginTop:0, marginLeft:0, fontSize:23}}>Good Evening, {avatar.name}</Text>
          <Text style={{fontWeight:"200", color:'#333', padding:10, fontSize:10, marginLeft:0}}>Lorem ipsum dolor sit amet.{"\n"}Lorem ipsum dolor sit amet fwrafh asfkah.</Text>
		      <Text style={{fontWeight:"200", color:'#bbb', padding:10, fontSize:10, marginLeft:0}}>Lorem ipsum dolor sit amet.{"\n"}Lorem ipsum dolor sit amet fwrafh asfkah.</Text>
  		      <View style={{flexDirection:'row'}}>
            <View style={styles.gears}><Icon name="gear" size={20} color="#d3d3d3" /></View><TextInput underlineColorAndroid="rgba(0,0,0,0)" placeholder="# Unit ID (Unit Identification)" style={styles.inputBar} />
           </View>
		     </View>
         <View style={{flex:5, }}>
         <Text style={{fontWeight:'600', margin:15, fontSize:10, }}>*    *    *</Text>
         <View style={{flexDirection:'row'}}>
          <View style={{flex:2,}}>
          <TouchableOpacity>
            <Text style={{margin:10, fontWeight:'600', fontSize:10, marginBottom:0}}>Amount Projected</Text>
            <Text style={{color:"#666", margin:10, fontSize:10}}>Your pricing estimate is based on the blah blah blah factor. This means that for everytime you blah blah, you get a blah blah (copywritting gig).</Text>
            <Text style={{color:"#888", fontSize:10, margin:10}}>$$ ( $598.49 )</Text>
            <Icon style={{position:"absolute", right:10, top:50,}} name='check' size={40} color={colorScheme[1]} />
          </TouchableOpacity>
          <TouchableOpacity>
          <Text style={{margin:10, fontWeight:'600', fontSize:10, marginBottom:0}}>Duration Estimates</Text>
          <Text style={{color:"#666", margin:10, fontSize:10}}>Your timing estimate means how much time is going to go into each product inspection. The heavier the package, the more time it's gonna take to do a complete diagnostic.</Text>
          <Text style={{color:"#888", fontSize:10, margin:10}}> 3hrs 13mins</Text>
                      <Icon style={{position:"absolute", right:10, top:50,}} name='check' size={40} color={colorScheme[1]} />
          </TouchableOpacity>
          <TouchableOpacity>
          <View style={{flexDirection:'row', justifyContent:'space-between'}}>
            <View>
            <Text style={{margin:10, fontWeight:'600', fontSize:10, marginBottom:0}}>Port & Location</Text>
            <Text style={{color:"#666", margin:10, fontSize:10}}># {beach.code}</Text>
            <Text style={{color:"#888", fontSize:10, margin:10}}>{beach.unlocs[0]}</Text>
            <Text style={{color:"#888", fontSize:10, margin:10}}>{beach.country}</Text>
            </View>
            <View>
            <Text style={{margin:10, fontWeight:'600', fontSize:10, marginBottom:0}}>...</Text>
            <Text style={{color:"#666", margin:10, fontSize:10}}>{beach.timezone}</Text>
            <Text style={{color:"#888", fontSize:10, margin:10}}>{beach.city}, {beach.province}</Text>
            </View>
          </View>
                      <Icon style={{position:"absolute", right:10, top:50,}} name='check' size={40} color={colorScheme[1]} />
          </TouchableOpacity>
          </View>
            <View style={{flex:1, justifyContent:'space-between', alignItems:'center'}}>
            <Text>S</Text>
            <Text>O</Text>
            <Text>F</Text>
            <Text>i</Text>
            <Text>A</Text>
            </View>
  
         </View>
         </View>
	      </ScrollView>
            <TouchableOpacity onPress={this.props.queueNav} style={{ position:'absolute', right:0, top:80, borderRadius:15, margin:10, marginTop:this.props.activeNavBar, backgroundColor:colorScheme[0], width:40, height:40, justifyContent:'center', alignItems:'center'}}>
      <Icon name = {this.props.menu}  size = {27} style={{}} color ={colorScheme[1]} />
       </TouchableOpacity></View>);
  }
}

const styles = StyleSheet.create({
  inputBar: {
    margin:20,
    shadowOffset: {
            height: 2,
            width: 2,
        },
      shadowColor: '#222',
      shadowOpacity: 0.5,
      elevation: 4,
      borderWidth:1,
      borderColor:'#fff',
      
      marginLeft:0,
      borderLeftWidth:0,
      borderRadius:5,
      color:"#444", 
      width:240, height:45, backgroundColor:'#f7f7f7',alignSelf:'center', justifyContent:'center', padding:10
},
gears: {
    margin:20,
    
      height:30,
      alignSelf:'center', justifyContent:'center'}
});