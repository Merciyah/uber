import React from 'react';
import { StyleSheet, Text, View, StatusBar, TextInput, Switch } from 'react-native';
import data from '../../data_exchange/mock'
import colors from '../../data_exchange/lib/color'
import Icon from 'react-native-vector-icons/FontAwesome';

// From this little widget, we can set our payment details.
export default class Pay extends React.Component {

	// Could be more dynamic
	eachPaymentInput(title, placeholder){
		return(<View style={{}}></View>
			)
	}
	lightBlue(){
return(
		<View style={{flex:5, backgroundColor:colors[1], margin:10, shadowOffset: {
	            height: 2,
	            width: 2,
	        },
	      shadowColor: '#888',
	      shadowOpacity: 0.8, borderWidth:1, borderRadius:5, borderColor:'rgba(0,0,0,0.2)'}}>
	      <View style={{padding:20}}>
	      <View style={{flex:1}}>
	      <Text style={{color:colors[0], fontWeight:'700', fontSize:13}}>NAME</Text>
	      <TextInput placeholder='Gabriella Doe' placeholderTextColor={colors[4]}  style={{height:60, color:colors[0], fontSize:15}} />
	      </View>
	      <View style={{flex:1}}>
	      <Text style={{color:colors[0], fontWeight:'700', fontSize:13}}>CARD NUMBER</Text>
	      <TextInput placeholder='Card Number...' placeholderTextColor={colors[4]}  keyboardType='numeric' style={{height:60, color:colors[0], fontSize:15}} />
	      </View>
	      <View style={{flex:1}}>
	      <Text style={{color:colors[0], fontWeight:'700', fontSize:13}}>EXPIRY DATE</Text>
	      <TextInput placeholder='12/11' placeholderTextColor={colors[4]} keyboardType='numeric'  style={{height:60, color:colors[0], fontSize:15}} />
	      </View>
	      <View style={{flex:1}}>
	      <Text style={{color:colors[0], fontWeight:'700', fontSize:13}}>CVC</Text>
	      <TextInput placeholder='111' placeholderTextColor={colors[4]}  style={{height:60, color:colors[0], fontSize:15}} />
	      </View>
	      <View style={{justifyContent:'space-between', alignItems:'center', flexDirection:'row'}}>
	      <Text style={{color:colors[0], fontSize:16}}>SAVE THIS CARD</Text>
	      <Switch />
	      </View>
	      </View>
	      </View>)
		
	}

  render(){
    return(<View style={{flex:1,}}>
	         <View style={{flex:1}}></View>
	         <View style={{flex:1, flexDirection:'row', padding:10, justifyContent:'space-between'}}>
	         <Icon name='cc-stripe' color={colors[4]} size={40} />
	         <Text style={{margin:10, fontSize:15, fontWeight:'300', color:colors[1]}}>ADD NEW CARD</Text>
	         </View>
	         <View style={{flex:5, backgroundColor:colors[1], margin:10, shadowOffset: {
	            height: 2,
	            width: 2,
	        },
	      shadowColor: '#888',
	      shadowOpacity: 0.8, borderWidth:1, borderRadius:5, borderColor:'rgba(0,0,0,0.2)'}}>
	      <View style={{padding:20, flex:1}}>
	      <View style={{flex:1}}>
	      <Text style={{color:colors[0], fontWeight:'700', fontSize:13}}>NAME</Text>
	      <TextInput placeholder='Gabriella Doe' placeholderTextColor={colors[4]}  style={{height:60, color:colors[0], fontSize:15}} />
	      </View>
	      <View style={{flex:1}}>
	      <Text style={{color:colors[0], fontWeight:'700', fontSize:13}}>CARD NUMBER</Text>
	      <TextInput placeholder='Card Number...' placeholderTextColor={colors[4]}  keyboardType='numeric' style={{height:60, color:colors[0], fontSize:15}} />
	      </View>
	      <View style={{flex:1}}>
	      <Text style={{color:colors[0], fontWeight:'700', fontSize:13}}>EXPIRY DATE</Text>
	      <TextInput placeholder='12/11' placeholderTextColor={colors[4]} keyboardType='numeric'  style={{height:60, color:colors[0], fontSize:15}} />
	      </View>
	      <View style={{flex:1}}>
	      <Text style={{color:colors[0], fontWeight:'700', fontSize:13}}>CVC</Text>
	      <TextInput placeholder='111' keyboardType='numeric' placeholderTextColor={colors[4]}  style={{height:60, color:colors[0], fontSize:15}} />
	      </View>
	      <View style={{justifyContent:'space-between', alignItems:'center', flexDirection:'row'}}>
	      <Text style={{color:colors[0], fontSize:16}}>SAVE THIS CARD</Text>
	      <Switch />
	      </View>
	      </View>
	      </View>
	         <View style={{flex:1}}></View>
      </View>
    );
  }
} 

const styles = StyleSheet.create({
});