import React from 'react';
import { StyleSheet, Text, ListView, UIManager, Image, Platform, View, Dimensions, TouchableOpacity, LayoutAnimation, StatusBar } from 'react-native';
import colors from '../../data_exchange/lib/color'
var {height, width} = Dimensions.get('window');

import el1 from '../../imgs/gallery/zero.jpg'
import el2 from '../../imgs/gallery/19.jpg'
import el3 from '../../imgs/gallery/craftbuddy.jpg'
import el4 from '../../imgs/gallery/cuffs.jpg'
import el5 from '../../imgs/gallery/fil.jpg'
import el6 from '../../imgs/gallery/ggasby.jpg'
import el7 from '../../imgs/gallery/habibi.jpg'
import el8 from '../../imgs/gallery/owen.jpg'
import el9 from '../../imgs/gallery/sdwife.jpg'
import el10 from '../../imgs/gallery/seth.jpg'
import el11 from '../../imgs/gallery/torbine.jpg'
import el12 from '../../imgs/gallery/travelfriends.jpg'
import el13 from '../../imgs/gallery/wings.jpg'
import el14 from '../../imgs/gallery/brother.jpg'

var imagesArr = [
  el1,
  el2,
  el3,
  el4,
  el5,
  el6,
  el7,
  el8,
  el9,
  el10,
  el11,
  el12,
  el13,
  el14,
  ]

  var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
export default class Gallery extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      dataSource: ds.cloneWithRows(imagesArr)
    }


    // This is for android animation
    if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental &&   UIManager.setLayoutAnimationEnabledExperimental(true);
}
  }

eachImage(x){
  return(<Image source={x} style={{width: ((width/3) - 10), margin:5, borderRadius: 5, height:((width/3) - 10)}} />)
}
render(){
        LayoutAnimation.easeInEaseOut();
      return (
        <ListView
        dataSource = {this.state.dataSource}
        contentContainerStyle = {{flexDirection:'row', flexWrap:'wrap' }}
        renderRow = {(data) => this.eachImage(data)}
         />);
        }
}
        
   

const styles = StyleSheet.create({
});