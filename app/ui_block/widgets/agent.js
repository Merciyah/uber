import React from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
import data from '../../data_exchange/mock'
import colors from '../../data_exchange/lib/color'

export default class Agent extends React.Component {
  render() {
    return (
      <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
         <CreditCardInput inputStyle={{borderWidth:0}} cardScale = {1} onChange={this._onChange} />
         <Text style={{fontSize:20, color:colors[3], }}>Welcome to the Agent view</Text>

      </View>
    );
  }
} 

const styles = StyleSheet.create({
});