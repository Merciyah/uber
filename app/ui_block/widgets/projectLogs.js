// Theses a lot of state manipulation going on here.
/*
Okay so we have...
error logs
1. agent profile as a push nav
2. Images and messages 
3. Product Details
- change scrollview to Listview for GET api call
*/
import React from 'react';
import { StyleSheet, Text, View, Dimensions, StatusBar, Platform, UIManager, ScrollView, Image, ImageBackground, LayoutAnimation, TouchableOpacity } from 'react-native';
import colors from '../../data_exchange/lib/color'
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  ProviderPropType,
  Animated as AnimatedMap,
  AnimatedRegion,
  Marker,
} from 'react-native-maps';

var {height, width} = Dimensions.get('window');
import data from '../../data_exchange/lib/ports'
import Logths from '../../data_exchange/coordinateDelta'
import Gallery from './gallery'

import stock1 from '../../imgs/stock1.jpg'
import stock2 from '../../imgs/stock2.jpg'
import stock3 from '../../imgs/stock3.jpg'
import bird from '../../imgs/bird.png'
// List of jobs
var jobs = [{}]

//list of coordinates
var friend1 = data.COPBO
var friend2 = data.GBBAD
var friend3 = data.ILNAT
var friend4 = data.USOAK
var friend5 = data.USHOU
var friend6 = data.TRAVC
var friend7 = data.ARBHI
var friend8 = data.NLAMS
var friend9 = data.NGWAR

// Regions

var region1 = {
      latitude: friend1.coordinates[0],
      longitude: friend1.coordinates[1],
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    }

var region2 = {
      latitude: friend1.coordinates[0],
      longitude: friend1.coordinates[1],
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    }

// This array is to distinguish between on widgets an off widgets
export default class Settings extends React.Component {
	constructor(props){
		super(props)

    console.log(Logths)
		this.state = {
			check:'check',
      region: region1,
      lat: 37.78825,
      long: -122.4324,
      isGalleryActive:false,
      map: {
        isToggled: false,
        icon: 'map-o',
        color:colors[0]
      },
      gallery: {
        isToggled: false,
        icon: 'bar-chart-o',
        color:colors[0]
      },
      isMapVisible: false,
      message: {
        isToggled: false,
        icon: 'envelope-o',
        color:colors[0]
      },
		}

      if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental &&   UIManager.setLayoutAnimationEnabledExperimental(true);
}
	}


  toggleGallery(){
    if(this.state.gallery.isToggled){
        this.setState({
      gallery: {
        isToggled: false,
        icon: 'bar-chart-o',
        color:colors[0]
      },
    })
    }else{
        this.setState({
         gallery: {
        isToggled: true,
        icon: 'grid-o',
        color:colors[4]
      },
        })}
  }
  toggleMap(){
    if(this.state.map.isToggled){
        this.setState({
      map: {
        isToggled: false,
        icon: 'map-o',
        color:colors[0]
      },
    })
    }else{
        this.setState({
         map: {
        isToggled: true,
        icon: 'map-o',
        color:colors[4]
      },
        })}
  }

  regionFrom(lat, lon, distance) {
        distance = distance/2
        const circumference = 40075
        const oneDegreeOfLatitudeInMeters = 111.32 * 1000
        const angularDistance = distance/circumference

        const latitudeDelta = distance / oneDegreeOfLatitudeInMeters
        const longitudeDelta = Math.abs(Math.atan2(
                Math.sin(angularDistance)*Math.cos(lat),
                Math.cos(angularDistance) - Math.sin(lat) * Math.sin(lat)))

        return result = {
            latitude: lat,
            longitude: lon,
            latitudeDelta,
            longitudeDelta,
        }
    }

    gallery(){
      if(this.state.gallery.isToggled){
        return(<Gallery />)
      }else{
        return(<View />)
      }
    }

  map(){
    console.log(this.regionFrom(this.state.lat, this.state.long, 1050))
    // This is a realtime update on your Handlers Location
    if(this.state.map.isToggled){
        return(<View styles={{flex:1, position:'absolute', top:0, shadowOffset: {
                height: 2,
                width: 2,
            },
          shadowColor: '#222',
          elevation: 3,
          shadowOpacity: 0.6, padding:10}}><AnimatedMap
          region={{
            latitude: this.state.lat,
            longitude: this.state.long,
            latitudeDelta: this.regionFrom(this.state.lat, this.state.long, 3000).latitudeDelta,
            longitudeDelta: this.regionFrom(this.state.lat, this.state.long, 3000).longitudeDelta
          }}
          onRegionChange={this.onRegionChange}
        style = {{width:width, height:height, borderColor:colors[4], borderRadius:5 }}>
        <Marker
            onPress={() => this.setState({ marker1: !this.state.marker1 })}
            coordinate={{
              latitude: this.state.lat,
              longitude: this.state.long
            }} />
    
        </AnimatedMap>
            <TouchableOpacity onPress = {() => this.toggleMap()} style={{position:'absolute', borderRadius:5, top:5, left:5, width:40, height:40, alignItems:'center', justifyContent:'center', backgroundColor:colors[1]}}><Icon name='close' size={20} color="#fff" /></TouchableOpacity>
        <View style={{position:'absolute', bottom:0, left:0, width:width, height:200, flexDirection:'row', alignItems:'center', justifyContent:'space-around', marginTop:20}}>
        <View style={{}}>
        <TouchableOpacity onPress={() => this.setState({lat: friend1.coordinates[1], long: friend1.coordinates[0]})} style={{width:100, height:40, padding:7, margin:2, marginBottom:10, borderRadius:5, borderWidth:1, borderColor:'#fff', backgroundColor:colors[3], justifyContent:'center'}}><Text style={{color:"#fff", alignItems:'center',fontSize:9, fontWeight:'700'}}>{friend1.country.toUpperCase()}{'\n'}{friend1.city.toUpperCase()}</Text></TouchableOpacity>
        <TouchableOpacity onPress={() => this.setState({lat: friend2.coordinates[1], long: friend2.coordinates[0]})} style={{width:100, height:40, padding:7, margin:2, marginBottom:10, borderRadius:5, borderWidth:1, borderColor:'#fff', backgroundColor:colors[3], justifyContent:'center'}}><Text style={{color:"#fff", alignItems:'center',fontSize:9, fontWeight:'700'}}>{friend2.country.toUpperCase()}{'\n'}{friend2.city.toUpperCase()}</Text></TouchableOpacity>
        <TouchableOpacity onPress={() => this.setState({lat: friend3.coordinates[1], long: friend3.coordinates[0]})} style={{width:100, height:40, padding:7, margin:2, marginBottom:10, borderRadius:5, borderWidth:1, borderColor:'#fff', backgroundColor:colors[3], justifyContent:'center'}}><Text style={{color:"#fff", alignItems:'center',fontSize:9, fontWeight:'700'}}>{friend3.country.toUpperCase()}{'\n'}{friend3.city.toUpperCase()}</Text></TouchableOpacity>
        </View>
        <View style={{}}>
        <TouchableOpacity onPress={() => this.setState({lat: friend4.coordinates[1], long: friend4.coordinates[0]})} style={{width:100, height:40, padding:7, margin:2, marginBottom:10, borderRadius:5, borderWidth:1, borderColor:'#fff', backgroundColor:colors[3], justifyContent:'center'}}><Text style={{color:"#fff", alignItems:'center',fontSize:9, fontWeight:'700'}}>{friend4.country.toUpperCase()}{'\n'}{friend4.city.toUpperCase()}</Text></TouchableOpacity>
        <TouchableOpacity onPress={() => this.setState({lat: friend5.coordinates[1], long: friend5.coordinates[0]})} style={{width:100, height:40, padding:7, margin:2, marginBottom:10, borderRadius:5, backgroundColor:colors[3], justifyContent:'center'}}><Text style={{color:"#fff", alignItems:'center',fontSize:9, fontWeight:'700'}}>{friend5.country.toUpperCase()}{'\n'}{friend5.city.toUpperCase()}</Text></TouchableOpacity>
        <TouchableOpacity onPress={() => this.setState({lat: friend6.coordinates[1], long: friend6.coordinates[0]})} style={{width:100, height:40, padding:7, margin:2, marginBottom:10, borderRadius:5, borderWidth:1, borderColor:'#fff', backgroundColor:colors[3], justifyContent:'center'}}><Text style={{color:"#fff", alignItems:'center',fontSize:9, fontWeight:'700'}}>{friend6.country.toUpperCase()}{'\n'}{friend6.city.toUpperCase()}</Text></TouchableOpacity>
        </View>
          <View style={{}}>
          <TouchableOpacity onPress={() => this.setState({lat: friend7.coordinates[1], long: friend7.coordinates[0]})} style={{width:100, height:40, padding:7, margin:2, marginBottom:10, borderRadius:5, borderWidth:1, borderColor:'#fff', backgroundColor:colors[3], justifyContent:'center'}}><Text style={{color:"#fff", alignItems:'center',fontSize:9, fontWeight:'700'}}>{friend7.country.toUpperCase()}{'\n'}{friend7.city.toUpperCase()}</Text></TouchableOpacity>
          <TouchableOpacity onPress={() => this.setState({lat: friend8.coordinates[1], long: friend8.coordinates[0]})} style={{width:100, height:40, padding:7, margin:2, marginBottom:10, borderRadius:5, borderWidth:1, borderColor:'#fff', backgroundColor:colors[3], justifyContent:'center'}}><Text style={{color:"#fff", alignItems:'center',fontSize:9, fontWeight:'700'}}>{friend8.country.toUpperCase()}{'\n'}{friend8.city.toUpperCase()}</Text></TouchableOpacity>
          <TouchableOpacity onPress={() => this.setState({lat: friend9.coordinates[1], long: friend9.coordinates[0]})} style={{width:100, height:40, padding:7, margin:2, marginBottom:10, borderRadius:5, borderWidth:1, borderColor:'#fff', backgroundColor:colors[3], justifyContent:'center'}}><Text style={{color:"#fff", alignItems:'center',fontSize:9, fontWeight:'700'}}>{friend9.country.toUpperCase()}{'\n'}{friend9.city.toUpperCase()}</Text></TouchableOpacity>
          </View>
        </View>
    
        </View>)
      }else{
          return(<View />)
        }
    }

	eachRow(time){
		if(time == 0){
			time = "Just a Moment"
		}else{
			time= time + " minutes ago"
		}
		return(<View style={{height:50, borderRadius:5, alignItems:'center', flexDirection:'row',  justifyContent:'space-between', padding:5,}}>
			<View style={{height:30, width:30, borderRadius:15, justifyContent:'center', alignItems:'center', borderWidth:1, borderColor:colors[0]}}>
			<Icon color={colors[0]} style={{}} name = {this.state.check} size={13} />
			</View>
			<View style={{flex:1, padding:5, justifyContent:'center', marginRight:5}}>
			<Text style={{color:colors[0], fontWeight:'600', fontSize:14}}>New Image</Text>
			<Text style={{color:colors[0], fontWeight:'300', fontSize:10}}>{time}</Text>
			</View>
			</View>)
	}

  render() {
    LayoutAnimation.spring();
    return (
    	<View style={{width:width, height:height, marginLeft:-20, justifyContent:'center', backgroundColor:'rgba(255,255,255,0.6)',}}>
    	<ScrollView>
      <StatusBar hidden ={true} />
      <TouchableOpacity onPress={this.props.close} style={{width:30, height:30, borderRadius:5, justifyContent:'center', alignItems:"center", backgroundColor:colors[4]}}>
      <Icon name ="close" size={15} color={colors[0]} />
      </TouchableOpacity>
    	<View style={{height:100, padding:20, marginBottom:0, marginTop:10, backgroundColor:colors[4],shadowOffset: {
            height: 2,
            width: 2,
        },
      shadowColor: '#222',
      elevation: 3,
      shadowOpacity: 0.5, margin:5, borderRadius:4}}>
      	<Text style={{fontSize:24, color:colors[0]}}>{this.props.uiData.deligate}</Text>
      	<Text style={{fontSize:10, color:colors[0]}}>No need to fear, {this.props.uiData.deligate} is on the case.</Text><View style={{justifyContent:'flex-end', alignSelf:'flex-end', flex:1, flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
      </View>
      </View>
      <View style={{flexDirection:'row',margin:10, marginTop:0, alignItems:'center', justifyContent:'flex-start'}}>
      
      <TouchableOpacity style={styles.mechanisburg}><Icon name = 'envelope-o' size ={13} color = {colors[0]} /></TouchableOpacity>
      <TouchableOpacity onPress = {() => this.toggleGallery()} style={styles.mechanisburg}><Icon name = 'bar-chart-o' size ={13} color = {this.state.gallery.color} /></TouchableOpacity>
      <TouchableOpacity onPress={() => this.toggleMap()} style={styles.mechanisburg}>
      <Icon name = {this.state.map.icon} size ={15} color = {this.state.map.color}/>
      </TouchableOpacity>
      </View>
      <View style={{justifyContent:'center', alignItems:'center'}}>
      
      {this.gallery()}
      </View>
      </ScrollView>
      {this.map()}
      </View>
    );
  }
} 


const styles = StyleSheet.create({
  mechanisburg:{
    backgroundColor:colors[1],
    padding:15,
    paddingTop:15,
    margin:20,
    marginTop:-1,
    marginLeft:0

  }
});