import React from 'react';
import { StyleSheet, Text, UIManager, Platform, View, TouchableOpacity, LayoutAnimation, StatusBar } from 'react-native';
import colors from '../../data_exchange/lib/color'

const ironThat = [""]

export default class Nav extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      top:0,
      color:[colors[0], colors[0], colors[0]]
    }

    this.state.color[this.props.val] = colors[3]

    // This is for android animation
    if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental &&   UIManager.setLayoutAnimationEnabledExperimental(true);
}
  }

  

    render() {
        LayoutAnimation.easeInEaseOut();
      return (
        <View style={{height:70, backgroundColor:colors[4],marginTop:this.props.navTop, alignItems:'center', flexDirection:'row', padding:5, justifyContent:'space-between'}}>
           <TouchableOpacity onPress={() => this.props.navigator.replace({id:'book'})} style={{flex:1, padding:10, }}>
           <Text style={{fontSize:15, marginBottom:3, color:this.state.color[0], fontWeight:'400' }}>Book</Text>
           <Text style={{fontSize:7, color:colors[0], }}>Lorem ipsum dolor sit amet. Lorem.</Text>
           </TouchableOpacity>
           <TouchableOpacity onPress={() => this.props.navigator.replace({id:'qclogs'})} style={{flex:1, padding:10, }}>
           <Text style={{fontSize:15, marginBottom:3, color:this.state.color[1], fontWeight:'400' }}>QC Logs</Text>
           <Text style={{fontSize:7, color:colors[0], }}>Lorem ipsum dolor sit amet. Lorem ipsum.</Text>
           </TouchableOpacity>
           <TouchableOpacity style={{flex:1, padding:10, }}>
           <Text style={{fontSize:15, marginBottom:3, color:this.state.color[2], fontWeight:'400' }}>Settings</Text>
           <Text style={{fontSize:7, color:colors[0], }}>Lorem ipsum dolor sit amet. Lorem</Text>
           </TouchableOpacity>
        </View>);
      }
  
} 

const styles = StyleSheet.create({
});