import React from 'react';
import { StyleSheet, UIManager, Platform, Text, View, StatusBar } from 'react-native';
import data from '../data_exchange/mock'
import {Navigator} from 'react-native-deprecated-custom-components'

import QClogs from './qcLogs'
import Book from './book'
import Welcome from './welcome'
import Settings from './settings'


const routes = {
  qclogs: QClogs,
  book: Book,
  welcome: Welcome,
  settings: Settings
};

export default class Navi extends React.Component {
	constructor(){
		super()

		this.state = {
			activeNavBar:-70,
			menu: 'angle-double-down'

		}
}
	componentDidUpdate(){

	}

  renderScene({id}, navigator){
    const Scene = routes[id]
    return <Scene {...this.props} activeNavBar ={this.state.activeNavBar} menu ={this.state.menu} queueNav = {() => this.queueNav()} navigator={navigator}/>
  }

  queueNav(){
  	if(this.state.activeNavBar == -70){
  	  	this.setState({
  	  		activeNavBar: 0,
  	  		menu: "angle-double-up"
  	  	})}else{
  	  		this.setState({
  	  		activeNavBar: -70,
  	  		menu: "angle-double-down"
  	  	})
  	  	}
  }


  render() {
    return (
    	<View style={{flex:1}}>
         <Navigator
                  style={{flex: 1}}
                  ref={'NAV'}
                  initialRoute={{id: 'welcome', name: 'welcome'}}
                  renderScene={this.renderScene.bind(this)}
                /> 
      </View>
    );
  }
} 