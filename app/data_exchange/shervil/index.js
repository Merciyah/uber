import React from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
// React native already works with state. Why loop in more dependences than we need to. 

var pointOfInterest: {
        	val : 1287.97,
        	keyboard: 'numeric',
        	title: "Amount",
        	subText:'Your pricing estimate is based on the blah blah blah factor. This means that for everytime you blah blah, you get a blah blah (copywritting gig).'
      }
var duration: {
        val : 100,
        title: "Duration Estaimates",
        keyboard: "numeric",
        subText:"Your timing estimate means how much time is going to go into each product inspection. The heavier the package, the more time it's gonna take to do a complete diagnostic."
      }

class Shervil extends React.Component {
	constructor(props){
		super(props)

		// This has all our app state
	this.state = {
		setAmount: true,
		setDuration: true,
		rotate: "0deg",
		mission: pointOfInterest

		}
	}

	amendMission(){
		this.setState({
			mission: pointOfInterest
		})
	}

	amendMission2(){
		this.setState({
			mission: pointOfInterest
		})
	}

	// All app reducing technology happens right here.
} 

export default Shervil