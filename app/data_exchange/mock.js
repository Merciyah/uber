import charl from '../imgs/charlie.jpg'

var avatar = {
	name : 'Merciyah',
	location: "Oakland California",

	billing : {
		cardkey: "2dakhg8w320f2fh",
		provider:'Visa',
		exp: "12/70"
	}
};

var book = {

}

// This pulls a 50x50 photo from an external api
// ratings are on a 5.0 scale

var acceptedBy = {
	name: 'Charlie Hoehn',
	jobs_completed: 1350,
	average_review: 4.3,
	photo: charl,
	contact:{
		number: "08033437500",
		email : 'charl@xyz.com'
	},
	location: {
		city: "Lagos",
		country: "Nigeria",
		gps:{
			lat: 33,
			long:33
		}
	}
}

export default avatar